# FastAPI Book API Documentation

This documentation provides an overview of the Book API developed using FastAPI. Below are the endpoints and their functionalities.

## Endpoints

### Get All Books

- **URL:** `/`
- **Method:** `GET`
- **Description:** Retrieves all books stored in the database.
- **Response:** List of books.

### Create a Book

- **URL:** `/create`
- **Method:** `POST`
- **Description:** Creates a new book entry in the database.
- **Request Body:** JSON object representing the book to be created.
  - `id`: UUID (Universally Unique Identifier) of the book.
  - `title`: Title of the book (min length: 1 character).
  - `author`: Author of the book (min length: 1 character).
  - `description`: Description of the book (min length: 1 character, max length: 100 characters).
  - `rating`: Rating of the book (greater than 0, less than or equal to 10).
- **Response:** JSON object representing the created book.

### Update a Book

- **URL:** `/book/{id}`
- **Method:** `PUT`
- **Description:** Updates an existing book in the database.
- **Request Parameters:** `id` - ID of the book to be updated.
- **Request Body:** JSON object representing the updated book details.
  - `title`: Updated title of the book (min length: 1 character).
  - `author`: Updated author of the book (min length: 1 character).
  - `description`: Updated description of the book (min length: 1 character, max length: 100 characters).
  - `rating`: Updated rating of the book (greater than 0, less than or equal to 10).
- **Response:** JSON object representing the updated book.

### Delete a Book

- **URL:** `/book/{id}`
- **Method:** `DELETE`
- **Description:** Deletes a book from the database.
- **Request Parameters:** `id` - ID of the book to be deleted.
- **Response:** Success message indicating the deletion of the book.

## Data Models

### Book Model

Represents the structure of a book entity.

```json
{
  "id": "UUID",
  "title": "string",
  "author": "string",
  "description": "string",
  "rating": "integer"
}
