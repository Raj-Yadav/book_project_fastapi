from fastapi import FastAPI, HTTPException, Depends
from pydantic import BaseModel, Field
from uuid import UUID
import models
from database import engine, SessionLocal
from sqlalchemy.orm import Session



app = FastAPI()

models.Base.metadata.create_all(bind=engine)


def get_db():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()




class Book(BaseModel):
    id : UUID
    title : str = Field(min_length=1)
    author : str = Field(min_length=1)
    description : str = Field(min_length=1, max_length=100)
    rating : int = Field(gt=0, le=10)


BOOKS = []


@app.get("/")
def read_books(db: Session = Depends(get_db)):
    return db.query(models.Books).all()


@app.post("/create")
def create_book(book : Book, db : Session = Depends(get_db)):
    
    book_model = models.Books()
    book_model.title = book.title
    book_model.author = book.author
    book_model.description = book.description
    book_model.rating = book.rating

    db.add(book_model)
    db.commit()

    return book


@app.put("/book/{id}")
def update_book(book_id: int, book : Book, db: Session = Depends(get_db)):
    book_model = db.query(models.Books).filter(models.Books.id == book_id).first()

    if book_model is None:
        raise HTTPException(status_code=404, detail=f"book id {id} doesn't exists in database")
    
    book_model.title = book.title
    book_model.author = book.author
    book_model.description = book.description
    book_model.rating = book.rating

    db.add(book_model)
    db.commit()

    return book


@app.delete("/book/{id}")
def delete_book(book_id : int, db: Session = Depends(get_db)):
    book_model = db.query(models.Books).filter(models.Books.id == book_id).first()
    if book_model is None:
        return HTTPException(status_code=404, detail=f"book id {id} doesn't exists in database")

    db.query(models.Books).filter(models.Books.id == book_id).delete()
    db.commit()
    return (f"book with id {book_id} deleted successfully !")
